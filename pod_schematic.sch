<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="9.2.2">
<drawing>
<settings>
<setting alwaysvectorfont="no"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="no" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="15" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="88" name="SimResults" color="9" fill="1" visible="yes" active="yes"/>
<layer number="89" name="SimProbes" color="9" fill="1" visible="yes" active="yes"/>
<layer number="90" name="Modules" color="5" fill="1" visible="yes" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="ATMEGA328PB-AUR">
<packages>
<package name="QFP80P900X900X120-32N">
<wire x1="3.5" y1="3.5" x2="3.5" y2="-3.5" width="0.127" layer="51"/>
<wire x1="3.5" y1="-3.5" x2="-3.5" y2="-3.5" width="0.127" layer="51"/>
<wire x1="-3.5" y1="-3.5" x2="-3.5" y2="3.5" width="0.127" layer="51"/>
<wire x1="-3.5" y1="3.5" x2="3.5" y2="3.5" width="0.127" layer="51"/>
<text x="-3.31528125" y="5.38633125" size="0.815803125" layer="25">&gt;NAME</text>
<text x="-3.402990625" y="-6.1588" size="0.81656875" layer="27">&gt;VALUE</text>
<circle x="-5.373" y="2.8" radius="0.1" width="0.2" layer="21"/>
<wire x1="-3.26" y1="3.5" x2="-3.5" y2="3.5" width="0.127" layer="21"/>
<wire x1="-3.5" y1="3.5" x2="-3.5" y2="3.26" width="0.127" layer="21"/>
<wire x1="-3.5" y1="-3.26" x2="-3.5" y2="-3.5" width="0.127" layer="21"/>
<wire x1="-3.5" y1="-3.5" x2="-3.26" y2="-3.5" width="0.127" layer="21"/>
<wire x1="3.26" y1="-3.5" x2="3.5" y2="-3.5" width="0.127" layer="21"/>
<wire x1="3.5" y1="-3.5" x2="3.5" y2="-3.26" width="0.127" layer="21"/>
<wire x1="3.5" y1="3.26" x2="3.5" y2="3.5" width="0.127" layer="21"/>
<wire x1="3.5" y1="3.5" x2="3.26" y2="3.5" width="0.127" layer="21"/>
<wire x1="-3.75" y1="3.33" x2="-5.16" y2="3.33" width="0.05" layer="39"/>
<wire x1="-5.16" y1="3.33" x2="-5.16" y2="-3.33" width="0.05" layer="39"/>
<wire x1="-5.16" y1="-3.33" x2="-3.75" y2="-3.33" width="0.05" layer="39"/>
<wire x1="-3.75" y1="-3.33" x2="-3.75" y2="-3.75" width="0.05" layer="39"/>
<wire x1="-3.75" y1="-3.75" x2="-3.33" y2="-3.75" width="0.05" layer="39"/>
<wire x1="-3.33" y1="-3.75" x2="-3.33" y2="-5.16" width="0.05" layer="39"/>
<wire x1="-3.33" y1="-5.16" x2="3.33" y2="-5.16" width="0.05" layer="39"/>
<wire x1="3.33" y1="-5.16" x2="3.33" y2="-3.75" width="0.05" layer="39"/>
<wire x1="3.33" y1="-3.75" x2="3.75" y2="-3.75" width="0.05" layer="39"/>
<wire x1="3.75" y1="-3.75" x2="3.75" y2="-3.33" width="0.05" layer="39"/>
<wire x1="3.75" y1="-3.33" x2="5.16" y2="-3.33" width="0.05" layer="39"/>
<wire x1="5.16" y1="-3.33" x2="5.16" y2="3.33" width="0.05" layer="39"/>
<wire x1="5.16" y1="3.33" x2="3.75" y2="3.33" width="0.05" layer="39"/>
<wire x1="3.75" y1="3.33" x2="3.75" y2="3.75" width="0.05" layer="39"/>
<wire x1="3.75" y1="3.75" x2="3.33" y2="3.75" width="0.05" layer="39"/>
<wire x1="3.33" y1="3.75" x2="3.33" y2="5.16" width="0.05" layer="39"/>
<wire x1="-3.75" y1="3.33" x2="-3.75" y2="3.75" width="0.05" layer="39"/>
<wire x1="-3.75" y1="3.75" x2="-3.33" y2="3.75" width="0.05" layer="39"/>
<wire x1="-3.33" y1="3.75" x2="-3.33" y2="5.16" width="0.05" layer="39"/>
<wire x1="-3.33" y1="5.16" x2="3.33" y2="5.16" width="0.05" layer="39"/>
<smd name="1" x="-4.17" y="2.8" dx="1.47" dy="0.55" layer="1"/>
<smd name="2" x="-4.17" y="2" dx="1.47" dy="0.55" layer="1"/>
<smd name="3" x="-4.17" y="1.2" dx="1.47" dy="0.55" layer="1"/>
<smd name="4" x="-4.17" y="0.4" dx="1.47" dy="0.55" layer="1"/>
<smd name="5" x="-4.17" y="-0.4" dx="1.47" dy="0.55" layer="1"/>
<smd name="6" x="-4.17" y="-1.2" dx="1.47" dy="0.55" layer="1"/>
<smd name="7" x="-4.17" y="-2" dx="1.47" dy="0.55" layer="1"/>
<smd name="8" x="-4.17" y="-2.8" dx="1.47" dy="0.55" layer="1"/>
<smd name="9" x="-2.8" y="-4.17" dx="0.55" dy="1.47" layer="1"/>
<smd name="10" x="-2" y="-4.17" dx="0.55" dy="1.47" layer="1"/>
<smd name="11" x="-1.2" y="-4.17" dx="0.55" dy="1.47" layer="1"/>
<smd name="12" x="-0.4" y="-4.17" dx="0.55" dy="1.47" layer="1"/>
<smd name="13" x="0.4" y="-4.17" dx="0.55" dy="1.47" layer="1"/>
<smd name="14" x="1.2" y="-4.17" dx="0.55" dy="1.47" layer="1"/>
<smd name="15" x="2" y="-4.17" dx="0.55" dy="1.47" layer="1"/>
<smd name="16" x="2.8" y="-4.17" dx="0.55" dy="1.47" layer="1"/>
<smd name="17" x="4.17" y="-2.8" dx="1.47" dy="0.55" layer="1"/>
<smd name="18" x="4.17" y="-2" dx="1.47" dy="0.55" layer="1"/>
<smd name="19" x="4.17" y="-1.2" dx="1.47" dy="0.55" layer="1"/>
<smd name="20" x="4.17" y="-0.4" dx="1.47" dy="0.55" layer="1"/>
<smd name="21" x="4.17" y="0.4" dx="1.47" dy="0.55" layer="1"/>
<smd name="22" x="4.17" y="1.2" dx="1.47" dy="0.55" layer="1"/>
<smd name="23" x="4.17" y="2" dx="1.47" dy="0.55" layer="1"/>
<smd name="24" x="4.17" y="2.8" dx="1.47" dy="0.55" layer="1"/>
<smd name="25" x="2.8" y="4.17" dx="0.55" dy="1.47" layer="1"/>
<smd name="26" x="2" y="4.17" dx="0.55" dy="1.47" layer="1"/>
<smd name="27" x="1.2" y="4.17" dx="0.55" dy="1.47" layer="1"/>
<smd name="28" x="0.4" y="4.17" dx="0.55" dy="1.47" layer="1"/>
<smd name="29" x="-0.4" y="4.17" dx="0.55" dy="1.47" layer="1"/>
<smd name="30" x="-1.2" y="4.17" dx="0.55" dy="1.47" layer="1"/>
<smd name="31" x="-2" y="4.17" dx="0.55" dy="1.47" layer="1"/>
<smd name="32" x="-2.8" y="4.17" dx="0.55" dy="1.47" layer="1"/>
</package>
</packages>
<symbols>
<symbol name="ATMEGA328PB-AUR">
<wire x1="12.7" y1="27.94" x2="12.7" y2="-27.94" width="0.254" layer="94"/>
<wire x1="12.7" y1="-27.94" x2="-12.7" y2="-27.94" width="0.254" layer="94"/>
<wire x1="-12.7" y1="-27.94" x2="-12.7" y2="27.94" width="0.254" layer="94"/>
<wire x1="-12.7" y1="27.94" x2="12.7" y2="27.94" width="0.254" layer="94"/>
<text x="-12.7591" y="29.3666" size="2.55361875" layer="95">&gt;NAME</text>
<text x="-12.7506" y="-31.899" size="2.55191875" layer="96">&gt;VALUE</text>
<pin name="VCC" x="17.78" y="22.86" length="middle" direction="pwr" rot="R180"/>
<pin name="GND" x="17.78" y="-25.4" length="middle" direction="pwr" rot="R180"/>
<pin name="PB0" x="-17.78" y="15.24" length="middle"/>
<pin name="PB1" x="-17.78" y="12.7" length="middle"/>
<pin name="PB2" x="-17.78" y="10.16" length="middle"/>
<pin name="PB3" x="-17.78" y="7.62" length="middle"/>
<pin name="PB4" x="-17.78" y="5.08" length="middle"/>
<pin name="PB5" x="-17.78" y="2.54" length="middle"/>
<pin name="PB6" x="-17.78" y="0" length="middle"/>
<pin name="PB7" x="-17.78" y="-2.54" length="middle"/>
<pin name="PC0" x="-17.78" y="-5.08" length="middle"/>
<pin name="PC1" x="-17.78" y="-7.62" length="middle"/>
<pin name="PC2" x="-17.78" y="-10.16" length="middle"/>
<pin name="PC3" x="-17.78" y="-12.7" length="middle"/>
<pin name="PC4" x="-17.78" y="-15.24" length="middle"/>
<pin name="PC5" x="-17.78" y="-17.78" length="middle"/>
<pin name="PC6/!RESET" x="-17.78" y="-20.32" length="middle"/>
<pin name="PD0" x="17.78" y="15.24" length="middle" rot="R180"/>
<pin name="PD1" x="17.78" y="12.7" length="middle" rot="R180"/>
<pin name="PD2" x="17.78" y="10.16" length="middle" rot="R180"/>
<pin name="PD3" x="17.78" y="7.62" length="middle" rot="R180"/>
<pin name="PD4" x="17.78" y="5.08" length="middle" rot="R180"/>
<pin name="PD5" x="17.78" y="2.54" length="middle" rot="R180"/>
<pin name="PD6" x="17.78" y="0" length="middle" rot="R180"/>
<pin name="PD7" x="17.78" y="-2.54" length="middle" rot="R180"/>
<pin name="AREF" x="-17.78" y="17.78" length="middle" direction="in"/>
<pin name="AVCC" x="17.78" y="25.4" length="middle" direction="pwr" rot="R180"/>
<pin name="PE0" x="17.78" y="-5.08" length="middle" rot="R180"/>
<pin name="PE1" x="17.78" y="-7.62" length="middle" rot="R180"/>
<pin name="PE2" x="17.78" y="-10.16" length="middle" rot="R180"/>
<pin name="PE3" x="17.78" y="-12.7" length="middle" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="ATMEGA328PB-AUR" prefix="U">
<description>ATmega Series 20 MHz 32 KB Flash 2 KB SRAM 8-Bit Microcontroller - TQFP-32</description>
<gates>
<gate name="G$1" symbol="ATMEGA328PB-AUR" x="0" y="0"/>
</gates>
<devices>
<device name="" package="QFP80P900X900X120-32N">
<connects>
<connect gate="G$1" pin="AREF" pad="20"/>
<connect gate="G$1" pin="AVCC" pad="18"/>
<connect gate="G$1" pin="GND" pad="5 21"/>
<connect gate="G$1" pin="PB0" pad="12"/>
<connect gate="G$1" pin="PB1" pad="13"/>
<connect gate="G$1" pin="PB2" pad="14"/>
<connect gate="G$1" pin="PB3" pad="15"/>
<connect gate="G$1" pin="PB4" pad="16"/>
<connect gate="G$1" pin="PB5" pad="17"/>
<connect gate="G$1" pin="PB6" pad="7"/>
<connect gate="G$1" pin="PB7" pad="8"/>
<connect gate="G$1" pin="PC0" pad="23"/>
<connect gate="G$1" pin="PC1" pad="24"/>
<connect gate="G$1" pin="PC2" pad="25"/>
<connect gate="G$1" pin="PC3" pad="26"/>
<connect gate="G$1" pin="PC4" pad="27"/>
<connect gate="G$1" pin="PC5" pad="28"/>
<connect gate="G$1" pin="PC6/!RESET" pad="29"/>
<connect gate="G$1" pin="PD0" pad="30"/>
<connect gate="G$1" pin="PD1" pad="31"/>
<connect gate="G$1" pin="PD2" pad="32"/>
<connect gate="G$1" pin="PD3" pad="1"/>
<connect gate="G$1" pin="PD4" pad="2"/>
<connect gate="G$1" pin="PD5" pad="9"/>
<connect gate="G$1" pin="PD6" pad="10"/>
<connect gate="G$1" pin="PD7" pad="11"/>
<connect gate="G$1" pin="PE0" pad="3"/>
<connect gate="G$1" pin="PE1" pad="6"/>
<connect gate="G$1" pin="PE2" pad="19"/>
<connect gate="G$1" pin="PE3" pad="22"/>
<connect gate="G$1" pin="VCC" pad="4"/>
</connects>
<technologies>
<technology name="">
<attribute name="DESCRIPTION" value=" AVR microcontroller; EEPROM: 1024B; SRAM: 2kB; Flash: 32kB; TQFP32 "/>
<attribute name="DIGI-KEY_PART_NUMBER" value="ATMEGA328PB-AURCT-ND"/>
<attribute name="DIGI-KEY_PURCHASE_URL" value="https://www.digikey.ca/product-detail/en/microchip-technology/ATMEGA328PB-AUR/ATMEGA328PB-AURCT-ND/5722706?utm_source=snapeda&amp;utm_medium=aggregator&amp;utm_campaign=symbol"/>
<attribute name="MF" value="Microchip"/>
<attribute name="MP" value="ATMEGA328PB-AUR"/>
<attribute name="PACKAGE" value="TQFP-32 Microchip"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="M41T00SM6F">
<packages>
<package name="SOIC127P600X175-8N">
<wire x1="-1.9812" y1="1.6764" x2="-1.9812" y2="2.1336" width="0" layer="51"/>
<wire x1="-1.9812" y1="2.1336" x2="-3.1496" y2="2.1336" width="0" layer="51"/>
<wire x1="-3.1496" y1="2.1336" x2="-3.1496" y2="1.6764" width="0" layer="51"/>
<wire x1="-3.1496" y1="1.6764" x2="-1.9812" y2="1.6764" width="0" layer="51"/>
<wire x1="-1.9812" y1="0.4064" x2="-1.9812" y2="0.8636" width="0" layer="51"/>
<wire x1="-1.9812" y1="0.8636" x2="-3.1496" y2="0.8636" width="0" layer="51"/>
<wire x1="-3.1496" y1="0.8636" x2="-3.1496" y2="0.4064" width="0" layer="51"/>
<wire x1="-3.1496" y1="0.4064" x2="-1.9812" y2="0.4064" width="0" layer="51"/>
<wire x1="-1.9812" y1="-0.8636" x2="-1.9812" y2="-0.4064" width="0" layer="51"/>
<wire x1="-1.9812" y1="-0.4064" x2="-3.1496" y2="-0.4064" width="0" layer="51"/>
<wire x1="-3.1496" y1="-0.4064" x2="-3.1496" y2="-0.8636" width="0" layer="51"/>
<wire x1="-3.1496" y1="-0.8636" x2="-1.9812" y2="-0.8636" width="0" layer="51"/>
<wire x1="-1.9812" y1="-2.1336" x2="-1.9812" y2="-1.6764" width="0" layer="51"/>
<wire x1="-1.9812" y1="-1.6764" x2="-3.1496" y2="-1.6764" width="0" layer="51"/>
<wire x1="-3.1496" y1="-1.6764" x2="-3.1496" y2="-2.1336" width="0" layer="51"/>
<wire x1="-3.1496" y1="-2.1336" x2="-1.9812" y2="-2.1336" width="0" layer="51"/>
<wire x1="1.9812" y1="-1.6764" x2="1.9812" y2="-2.1336" width="0" layer="51"/>
<wire x1="1.9812" y1="-2.1336" x2="3.1496" y2="-2.1336" width="0" layer="51"/>
<wire x1="3.1496" y1="-2.1336" x2="3.1496" y2="-1.6764" width="0" layer="51"/>
<wire x1="3.1496" y1="-1.6764" x2="1.9812" y2="-1.6764" width="0" layer="51"/>
<wire x1="1.9812" y1="-0.4064" x2="1.9812" y2="-0.8636" width="0" layer="51"/>
<wire x1="1.9812" y1="-0.8636" x2="3.1496" y2="-0.8636" width="0" layer="51"/>
<wire x1="3.1496" y1="-0.8636" x2="3.1496" y2="-0.4064" width="0" layer="51"/>
<wire x1="3.1496" y1="-0.4064" x2="1.9812" y2="-0.4064" width="0" layer="51"/>
<wire x1="1.9812" y1="0.8636" x2="1.9812" y2="0.4064" width="0" layer="51"/>
<wire x1="1.9812" y1="0.4064" x2="3.1496" y2="0.4064" width="0" layer="51"/>
<wire x1="3.1496" y1="0.4064" x2="3.1496" y2="0.8636" width="0" layer="51"/>
<wire x1="3.1496" y1="0.8636" x2="1.9812" y2="0.8636" width="0" layer="51"/>
<wire x1="1.9812" y1="2.1336" x2="1.9812" y2="1.6764" width="0" layer="51"/>
<wire x1="1.9812" y1="1.6764" x2="3.1496" y2="1.6764" width="0" layer="51"/>
<wire x1="3.1496" y1="1.6764" x2="3.1496" y2="2.1336" width="0" layer="51"/>
<wire x1="3.1496" y1="2.1336" x2="1.9812" y2="2.1336" width="0" layer="51"/>
<wire x1="-1.9812" y1="-2.5654" x2="1.9812" y2="-2.5654" width="0" layer="51"/>
<wire x1="1.9812" y1="-2.5654" x2="1.9812" y2="2.5654" width="0" layer="51"/>
<wire x1="1.9812" y1="2.5654" x2="0.3048" y2="2.5654" width="0" layer="51"/>
<wire x1="0.3048" y1="2.5654" x2="-0.3048" y2="2.5654" width="0" layer="51"/>
<wire x1="-0.3048" y1="2.5654" x2="-1.9812" y2="2.5654" width="0" layer="51"/>
<wire x1="-1.9812" y1="2.5654" x2="-1.9812" y2="-2.5654" width="0" layer="51"/>
<wire x1="0.3048" y1="2.5654" x2="-0.3048" y2="2.5654" width="0" layer="51" curve="-180"/>
<wire x1="-1.9812" y1="-2.5654" x2="1.9812" y2="-2.5654" width="0.1524" layer="21"/>
<wire x1="1.9812" y1="2.5654" x2="0.3048" y2="2.5654" width="0.1524" layer="21"/>
<wire x1="0.3048" y1="2.5654" x2="-0.3048" y2="2.5654" width="0.1524" layer="21"/>
<wire x1="-0.3048" y1="2.5654" x2="-1.9812" y2="2.5654" width="0.1524" layer="21"/>
<wire x1="0.3048" y1="2.5654" x2="-0.3048" y2="2.5654" width="0.1524" layer="21" curve="-180"/>
<text x="-3.46285" y="4.45586875" size="2.087890625" layer="25" ratio="10" rot="SR0">&gt;NAME</text>
<text x="-3.45773125" y="-5.720509375" size="2.084809375" layer="27" ratio="10" rot="SR0">&gt;VALUE</text>
<smd name="1" x="-2.794" y="1.905" dx="1.4224" dy="0.5334" layer="1"/>
<smd name="2" x="-2.794" y="0.635" dx="1.4224" dy="0.5334" layer="1"/>
<smd name="3" x="-2.794" y="-0.635" dx="1.4224" dy="0.5334" layer="1"/>
<smd name="4" x="-2.794" y="-1.905" dx="1.4224" dy="0.5334" layer="1"/>
<smd name="5" x="2.794" y="-1.905" dx="1.4224" dy="0.5334" layer="1"/>
<smd name="6" x="2.794" y="-0.635" dx="1.4224" dy="0.5334" layer="1"/>
<smd name="7" x="2.794" y="0.635" dx="1.4224" dy="0.5334" layer="1"/>
<smd name="8" x="2.794" y="1.905" dx="1.4224" dy="0.5334" layer="1"/>
</package>
</packages>
<symbols>
<symbol name="M41T00SM6F">
<wire x1="-15.24" y1="12.7" x2="-15.24" y2="-17.78" width="0.4064" layer="94"/>
<wire x1="-15.24" y1="-17.78" x2="15.24" y2="-17.78" width="0.4064" layer="94"/>
<wire x1="15.24" y1="-17.78" x2="15.24" y2="12.7" width="0.4064" layer="94"/>
<wire x1="15.24" y1="12.7" x2="-15.24" y2="12.7" width="0.4064" layer="94"/>
<text x="-5.70751875" y="14.3962" size="2.089359375" layer="95" ratio="10" rot="SR0">&gt;NAME</text>
<text x="-4.55443125" y="-20.4568" size="2.086390625" layer="96" ratio="10" rot="SR0">&gt;VALUE</text>
<pin name="VCC" x="-20.32" y="7.62" length="middle" direction="pwr"/>
<pin name="VBAT" x="-20.32" y="2.54" length="middle" direction="pwr"/>
<pin name="SDA" x="-20.32" y="-2.54" length="middle"/>
<pin name="SCL" x="-20.32" y="-5.08" length="middle" direction="in"/>
<pin name="XI" x="-20.32" y="-7.62" length="middle" direction="in"/>
<pin name="VSS" x="-20.32" y="-12.7" length="middle" direction="pas"/>
<pin name="XO" x="20.32" y="7.62" length="middle" direction="out" rot="R180"/>
<pin name="FT/OUT(1)" x="20.32" y="5.08" length="middle" direction="out" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="M41T00SM6F" prefix="U">
<description>Serial access real-time clock</description>
<gates>
<gate name="A" symbol="M41T00SM6F" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SOIC127P600X175-8N">
<connects>
<connect gate="A" pin="FT/OUT(1)" pad="7"/>
<connect gate="A" pin="SCL" pad="6"/>
<connect gate="A" pin="SDA" pad="5"/>
<connect gate="A" pin="VBAT" pad="3"/>
<connect gate="A" pin="VCC" pad="8"/>
<connect gate="A" pin="VSS" pad="4"/>
<connect gate="A" pin="XI" pad="1"/>
<connect gate="A" pin="XO" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="AVAILABILITY" value="Unavailable"/>
<attribute name="DESCRIPTION" value=" Real Time Clock Serial 8byte Clock/Calendar/Battery Backup 8-Pin SO N T/R "/>
<attribute name="MF" value="STMicroelectronics"/>
<attribute name="MP" value="M41T00SM6F"/>
<attribute name="PACKAGE" value="SO-8 STMicroelectronics"/>
<attribute name="PRICE" value="None"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
</classes>
<parts>
<part name="U1" library="ATMEGA328PB-AUR" deviceset="ATMEGA328PB-AUR" device=""/>
<part name="U2" library="M41T00SM6F" deviceset="M41T00SM6F" device=""/>
</parts>
<sheets>
<sheet>
<plain>
</plain>
<instances>
<instance part="U1" gate="G$1" x="33.02" y="53.34" smashed="yes">
<attribute name="NAME" x="20.2609" y="82.7066" size="2.55361875" layer="95"/>
<attribute name="VALUE" x="20.2694" y="21.441" size="2.55191875" layer="96"/>
</instance>
<instance part="U2" gate="A" x="99.06" y="68.58" smashed="yes">
<attribute name="NAME" x="93.35248125" y="82.9762" size="2.089359375" layer="95" ratio="10" rot="SR0"/>
<attribute name="VALUE" x="94.50556875" y="48.1232" size="2.086390625" layer="96" ratio="10" rot="SR0"/>
</instance>
</instances>
<busses>
</busses>
<nets>
</nets>
</sheet>
</sheets>
</schematic>
</drawing>
</eagle>
